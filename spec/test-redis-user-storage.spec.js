import {UserStorageService,
    RedisUserStorageService,
    UserStorageAccessConfiguration} from '../modules/storage/src';
import {ExpressDataApplication} from '@themost/express';
import {getApplication} from '@themost/test';


describe('RedisUserStorageService', () => {
    let application;
    beforeAll(done => {
        const app = getApplication();
        /**
         * @type {ExpressDataApplication}
         */
        application = app.get(ExpressDataApplication.name);
        // set mongo configuration
        // set mongo configuration
        application.getConfiguration().setSourceAt('settings/universis/storage/options', {
            "host": "localhost",
            "port": 6379
        });
        // use service
        application.useStrategy(UserStorageService, RedisUserStorageService);
        // add user access
        application.useService(UserStorageAccessConfiguration);
        /**
         * @type {UserStorageAccessConfiguration}
         */
        const userStorageAccessConfiguration = application.getConfiguration().getStrategy(UserStorageAccessConfiguration);
        userStorageAccessConfiguration.elements.push({
            "scope": [
                "registrar"
            ],
            "resource": "me/registrar/?",
            "access": [
                "read",
                "write"
            ]
        });
        return done();
    });

    it('should get RedisUserStorageService', () => {
        expect(application.getService(UserStorageService)).toBeInstanceOf(RedisUserStorageService);
    });

    it('should should RedisUserStorageService.setItem()', async () => {
        const context = application.createContext();
        context.user = {
            name: 'user1@example.com',
            authenticationScope: 'registrar'
        };
        /**
         * @type {UserStorageService}
         */
        const userStorageService = application.getService(UserStorageService);
        // // set item
        // await userStorageService.setItem(context, 'registrar', {
        //     departments: {
        //         active: {
        //             id: 100
        //         }
        //     }
        // });
        // set item
        await userStorageService.setItem(context, 'registrar/departments/active', {
            id: 101
        });
        // get item
        const item = await userStorageService.getItem(context, 'registrar/departments/active');
        expect(item).toBeTruthy();
        expect(item.id).toBe(101);
    });
    it('should RedisUserStorageService.removeItem()', async () => {
        const context = application.createContext();
        context.user = {
            name: 'user1@example.com',
            authenticationScope: 'registrar'
        };
        /**
         * @type {UserStorageService}
         */
        const userStorageService = application.getService(UserStorageService);
        // remove item
        await userStorageService.removeItem(context, 'registrar');
        // get item
        const item = await userStorageService.getItem(context, 'registrar');
        expect(item).toBeFalsy();
    });
    it('should use RedisUserStorageService.clear()', async () => {
        const context = application.createContext();
        context.user = {
            name: 'user1@example.com',
            authenticationScope: 'registrar'
        };
        /**
         * @type {UserStorageService}
         */
        const userStorageService = application.getService(UserStorageService);
        // remove item
        await userStorageService.clear(context);
        // get item
        const item = await userStorageService.getItem(context, 'registrar');
        expect(item).toBeFalsy();
    });
});
