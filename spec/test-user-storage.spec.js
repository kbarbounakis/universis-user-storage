import {UserStorage,
    UserStorageService,
    RedisUserStorageService,
    UserStorageAccessConfiguration} from '../modules/storage/src';
import {ExpressDataApplication} from '@themost/express';
import {getApplication} from '@themost/test';

describe('UserStorageService', () => {

    let userStorage;
    beforeAll(done => {
        const app = getApplication();
        /**
         * @type {ExpressDataApplication}
         */
        const application = app.get(ExpressDataApplication.name);
        // set mongo configuration
        // set mongo configuration
        application.getConfiguration().setSourceAt('settings/universis/storage/options', {
            "host": "localhost",
            "port": 6379
        });
        // use service
        application.useStrategy(UserStorageService, RedisUserStorageService);
        // add user access
        application.useService(UserStorageAccessConfiguration);
        /**
         * @type {UserStorageAccessConfiguration}
         */
        const userStorageAccessConfiguration = application.getConfiguration().getStrategy(UserStorageAccessConfiguration);
        userStorageAccessConfiguration.elements.push({
            "scope": [
                "registrar"
            ],
            "resource": "me/registrar/?",
            "access": [
                "read",
                "write"
            ]
        });
        const context = application.createContext();
        context.user = {
            name: 'alexis.rees@example.com',
            authenticationScope: 'registrar'
        };
        userStorage = UserStorage.create(context);
        return done();
    });


    it('should use UserStorage.setItem()', async () => {
        await userStorage.setItem('registrar', {
            lastDepartment: '101',
            lastPath: '/requests/list'
        });
        const registrar = await userStorage.getItem('registrar');
        expect(registrar.lastDepartment).toBe('101');
        expect(registrar.lastPath).toBe('/requests/list');
        await userStorage.removeItem('registrar');
    });

    it('should use UserStorage.setItem() and add property', async () => {
        await userStorage.setItem('registrar', {
            lastDepartment: '101'
        });
        const registrar = await userStorage.getItem('registrar');
        expect(registrar.lastDepartment).toBe('101');
        await userStorage.setItem('registrar/lastPath', '/requests/list');
        const lastPath = await userStorage.getItem('registrar/lastPath');
        expect(lastPath).toBe('/requests/list');
        await userStorage.removeItem('registrar');
    });


    it('should use UserStorage.setItem() and add nested item property', async () => {
        await userStorage.removeItem('registrar');
        await userStorage.setItem('registrar/tables/Table1/tableProperty', 100 );
        await userStorage.setItem('registrar/tables/Table2/tableProperty', 101 );
        const table1 = await userStorage.getItem('registrar/tables/Table1');
        const table2 = await userStorage.getItem('registrar/tables/Table2');
        expect(table1).toBeTruthy();
        expect(table2).toBeTruthy();
        await userStorage.removeItem('registrar');
    });

    it('should use UserStorage.setItem() and set item property array', async () => {
        await userStorage.setItem('registrar', {
            lastDepartment: '101'
        });
        const registrar = await userStorage.getItem('registrar');
        expect(registrar.lastDepartment).toBe('101');
        await userStorage.setItem('registrar/lastPaths', [ '/requests/list', '/profile' ]);
        const lastPaths = await userStorage.getItem('registrar/lastPaths');
        expect(lastPaths).toBeInstanceOf(Array);
        expect(lastPaths[0]).toBe('/requests/list');
        await userStorage.removeItem('registrar');
    });

    it('should use UserStorage.removeItem()', async () => {
        let result = await userStorage.setItem('registrar', {
            lastDepartment: '101'
        });
        expect(result.lastDepartment).toBe('101');
        let registrar = await userStorage.getItem('registrar');
        expect(registrar.lastDepartment).toBe('101');
        await userStorage.setItem('registrar/lastPath', '/requests/list');
         await userStorage.removeItem('registrar/lastPath');
        registrar = await userStorage.getItem('registrar');
        expect(registrar.lastDepartment).toBe('101');
        expect(registrar.lastPath).toBeFalsy();
        await userStorage.removeItem('registrar');
    });

    it('should use UserStorage.getItem() and get access denied', async () => {
        try {
            await userStorage.getItem('application1');
            expect(false).toBeTruthy();
        }
        catch (err) {
            expect(err).toBeTruthy();
            expect(err.message).toBe('Access Denied');
        }
    });

});
