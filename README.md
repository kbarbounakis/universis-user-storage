# universis-user-storage

Universis API server extension for managing remote user storage that is going to be used to store user configuration data.

@universis/user-storage uses [MongoDB](https://www.mongodb.com/) as backend user storage by default.
Another option is to use [Redis JSON](https://oss.redislabs.com/redisjson/)

### MongoDB Installation

[Install MongoDB on Ubuntu](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)

[Install MongoDB on Windows](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/)

[Install MongoDB on MacOSX](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/)

[Enable Access Control](https://docs.mongodb.com/manual/tutorial/enable-authentication/)

### Redis Installation

Follow [RedisJSON](https://oss.redislabs.com/redisjson/) installation instructions
and install [Redis](https://redis.io/topics/quickstart) with RedisJSON plugin

[Launch RedisJSON with Docker](https://oss.redislabs.com/redisjson/#launch-redisjson-with-docker)

[Install Redis on Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-18-04)

[Install Redis on MacOSX](https://medium.com/@petehouston/install-and-config-redis-on-mac-os-x-via-homebrew-eb8df9a4f298)

[Install RedisJSON plugin on Ubuntu](https://oss.redislabs.com/redisjson/#building-and-loading-the-module)


### Installation

    npm i @universis/user-storage

### Configuration for MongoDB UserStorageService

Register MongoUserStorageService to application services section:

    # server/config/app.json

    {
        "services": [
            ...
            {
              "serviceType": "@universis/user-storage#UserStorageService",
              "strategyType": "@universis/user-storage#MongoUserStorageService"
            }
        ]
    ...
    }

Configure @universis/user-storage to connect to Redis database:

    "settings": {
        "universis": {
                "storage": {
                    "options": {
                       "host":"localhost",
                       "port":27017,
                       "database":"local_storage",
                       "authenticationDatabase": "admin",
                       "user":"mongo",
                       "password":"password"
                   }
                },
            }
        }
     ...

For a complete list of available options read [node_redis documentation](https://github.com/NodeRedis/node_redis#options-object-properties)

### Configuration for Redis UserStorageService

Register RedisUserStorageService to application services section:

    # server/config/app.json

    {
        "services": [
            ...
            {
              "serviceType": "@universis/user-storage#UserStorageService",
              "strategyType": "@universis/user-storage#RedisUserStorageService"
            }
        ]
    ...
    }

Configure @universis/user-storage to connect to Redis database:

    "settings": {
        "universis": {
                "storage": {
                    "options": {
                        "host": "127.0.0.1"
                        "port": 6379
                    }
                },
            }
        }
     ...

For a complete list of available options read [node_redis documentation](https://github.com/NodeRedis/node_redis#options-object-properties)


### Configure User Storage Access

UserStorageService uses UserStorageAccessConfiguration strategy in order to define user access against storage:

It registers DefaultUserStorageAccessConfiguration strategy which contains a set of privileges for configuring the way that users act against storage.

DefaultUserStorageAccessConfiguration gets these privileges from a file named ```user.storage.access.json``` which may exist in application configuration directory (e.g. server/config/)

    # server/config/user.storage.access.json

    [
        {
            "scope": [
                "registrar"
            ],
            "resource": "me/registrar/?",
            "access": [
                "read",
                "write"
            ]
        },
        {
            "scope": [
                "teachers"
            ],
            "resource": "me/teachers/?",
            "access": [
                "read",
                "write"
            ]
        },
        {
            "scope": [
                "students"
            ],
            "resource": "me/students/?",
            "access": [
                "read",
                "write"
            ]
        }
    ]

Each privilege contains a set of authorization scopes given to a user context during login, a resource path which is a regular expression
that represents an expression of a user storage key path and a set of access rights (read or write) on it.

e.g. A logged-in user with scope ```registrar``` has read-write to user storage for all keys that exist under ```me/registrar``` path
or a user with scope ```teachers``` has read-write access for all keys unders ```me/teachers``` path etc.   

UserStorageService extends application service router by adding the following endpoints

##### Get user storage item

    POST /api/users/me/storage/get

    {
        "key": "string"
    }

Returns a user storage item based on the specified key path e.g.

    {
        "key": "application1/preferences",
        "value": {
            "lastAction": "/hello",
        }
    }

##### Set user storage item

    POST /api/users/me/storage/set

    {
        "key": "string",
        "value": {
            ...
        }
    }

Sets a user storage item to the specified key path e.g.

    {
        "key": "application1/preferences",
        "value": {
            "lastAction": "/helloWorld",
        }
    }
